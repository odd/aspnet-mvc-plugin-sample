# ASP.NET (Core) 插件化开发 - 示例代码

## ASP.NET MVC5 插件化开发简化示例

* 代码分支：[asp.net_mvc5](https://gitee.com/jamesfancy/aspnet-mvc-plugin-sample/tree/asp.net_mvc5)
* 相关阅读：[ASP.NET MVC 插件化开发简化方案](http://mp.weixin.qq.com/s/ZyPARZMPnRwejFYHWJOsIg)

## ASP.NET Core MVC 2.0 插件化开发

* 代码分支：[asp.net_core_2.0](https://gitee.com/jamesfancy/aspnet-mvc-plugin-sample/tree/asp.net_core_2.0)
* 相关阅读：[ASP.NET Core 2.0 插件化开发](https://segmentfault.com/a/1190000011091141)

## ASP.NET Core MVC 2.2 插件化开发

> ASP.NET Core MVC 2.1 也是一样

* 代码分支：[asp.net_core_2.2](https://gitee.com/jamesfancy/aspnet-mvc-plugin-sample/tree/asp.net_core_2.2)
* 相关阅读：[ASP.NET Core 2.2 下插件化开发的改进](https://segmentfault.com/a/1190000018624553)

----

微信公众号“边城客栈”是一个软件开发技术相关的公众号，敬请关注：

![微信公众号“边城客栈”](https://images.gitee.com/uploads/images/2019/0323/121347_299e9181_132983.png)
